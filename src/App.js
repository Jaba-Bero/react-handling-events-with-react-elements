import React from 'react';

function App() {
  return (
  // eslint-disable-next-line no-undef
  React.createElement(
    "h1",
    { style: { color: '#999', fontSize: "19px"} },
    "Solar system planets",
    <ul className='planets-list'>
      <li>Mercury</li>
      <li>Venus</li>
      <li>Earth</li>
      <li>Mars</li>
      <li>Jupiter</li>
      <li>Saturn</li>
      <li>Uranus</li>
      <li>Neptune</li>
    </ul>,
    <label className='switch' htmlFor='checkbox' >
      <input type="checkbox" id="checkbox" ></input>
      <div className='slider round' onClick={function changeTheme(element) {
        const body = document.querySelector('body');
        const ul = document.querySelector('ul');
        body.classList.toggle('dark');
        ul.classList.toggle('green');
      }}></div>
    </label>
    )
  )
}

export default App;
